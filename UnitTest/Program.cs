﻿/*
 * css2xpath (Ported Version) Unit Test Project
 * 
 * Version 1.0
 * 
 *** Contains source materials from original version: ***
 * 
 * Original version by Andrea Giammarchi (http://webreflection.blogspot.com/2009/04/vice-versa-sub-project-css2xpath.html)
 * Original also covered under 'MIT License'
 * 
 * [License for port]:
 * 
 * The MIT License

    Copyright (c) MostThingsWeb (http://mosttw.wordpress.com/)

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.
 * 
 * 
*/

using System;
using System.Collections.Generic;
using Microsoft.JScript.Vsa;
using System.Text;

namespace MostThingsWeb {
    public class Program {

        // I know it's deprecated: If you can think of a better way please email me: mostthingsweb@gmail.com
        private static VsaEngine Engine = VsaEngine.CreateEngine();

        static void Main(string[] args) {
            OutputBanner(new String[] {"css2xpath (C# Port)", "Unit Test","", " by: MostThingsWeb" });

            Console.WriteLine(css2xpath.Transform("td.url a"));
            Console.WriteLine("Press any key to start...");
            Console.ReadKey();

            Console.WriteLine();

            List<String> slickspeedExprs = new List<string>();
            List<String> complexExprs = new List<string>();

            // Taken from: http://mootools.net/slickspeed/
            slickspeedExprs.AddRange(new String[] {"body", "div", "body div", "div p", "div > p", "div + p", "div ~ p", "div[class^=exa][class$=mple]", "div p a", "div, p, a", ".note", "div.example", "ul.tocline2", "div.example, div.note", "#title", "h1#title", "ul.toc li.tocline2", "ul.toc > li.tocline2", "h1#title + div > p", "h1[id]:contains(Selectors)", "a[href][lang][class]", "div[class]", "div[class=example]", "div[class^=exa]", "div[class$=mple]", "div[class*=e]", "div[class|=dialog]", "div[class!=made_up]", "div[class~=example]", "div:not(.example)", "p:contains(.selectors)", "p:nth-child(even)", "p:nth-child(2n)", "p:nth-child(odd)", "p:nth-child(2n+1)", "p:nth-child(n)", "p:only-child", "p:last-child", "p:first-child"  });

            // My custom, complex selectors
            complexExprs.AddRange(new String[] { "#container div.browser iframe[src]", "li:nth-child(odd) ul.item", "one > two > three.four + five[six]:nth-child(7):last-child", "b.someClass + #someId[href=\"http://www.google.com/\"]", "a:nth-child(even).opened[test] + q > #seven" });

            // A list of expressions that failed
            List<String> slickspeedFailed = new List<string>();
            List<String> complexFailed = new List<string>();
            
            Console.WriteLine(" SlickSpeed Selector Test:");
            Console.WriteLine();

            DoTests(slickspeedExprs, slickspeedFailed);

            Console.WriteLine();

            Console.WriteLine(" Complex Selector Test:");
            Console.WriteLine();

            DoTests(complexExprs, complexFailed);

            Console.WriteLine();

            OutputBanner(new String[] { "Results:" });

            int slickCount = slickspeedExprs.Count;
            int slickFail = slickspeedFailed.Count;

            int complexCount = complexExprs.Count;
            int complexFail = complexFailed.Count;

            Console.WriteLine(" Slickspeed Selectors:");
            Console.WriteLine("   Expressions: " + slickCount + " | Succeeded: " + (slickCount - slickFail) + " | Failed: " + slickFail);

            Console.WriteLine(" Complex Selectors:");
            Console.WriteLine("   Expressions: " + complexCount + " | Succeeded: " + (complexCount - complexFail) + " | Failed: " + complexFail);
            Console.WriteLine();

            if (slickFail > 0) {
                Console.WriteLine("Slickspeed Selectors Failed:");

                foreach (String failed in slickspeedFailed)
                    Console.WriteLine(failed);
            }

            if (complexFail > 0) {
                Console.WriteLine("Complex Selectors Failed:");

                foreach (String failed in complexFailed)
                    Console.WriteLine(failed);
            }

            Console.ReadLine();
        }

        private static void DoTests(List<String> testSet, List<String> failed) {
            // Find longest expression, for formatting purposes
            int lineSpan = 0;

            foreach (String testExpr in testSet) {
                int len = testExpr.Length;
                if (len > lineSpan)
                    lineSpan = len;
            }

            // Add 15 to line span to add room for spacing
            lineSpan += 15;

            StringBuilder entry = new StringBuilder();

            foreach (String testExpr in testSet) {
                entry.Append("   " + testExpr);

                String original = EvaluateWithOriginal(testExpr);
                String port = EvaluateWithPort(testExpr);

                String result;

                if (original == port) {
                    result = "Success!";
                } else {
                    result = "Failed.";
                    failed.Add(testExpr);
                }

                // Calculate the room left on the line
                int roomLeft = lineSpan - (testExpr.Length + result.Length);

                // Add periods
                entry.Append('.', roomLeft);

                // Append result
                entry.Append(result);

                Console.WriteLine(entry.ToString());

                entry.Clear();
            }
        }

        private static String EvaluateWithPort(String css) {
            return css2xpath.Transform(css);
        }

        private static String EvaluateWithOriginal(String css) {
            // The original version, minified with Google Closure Compiler
            const String originalJS = "var css2xpath=function(){var d=[/\\[([^\\]~\\$\\*\\^\\|\\!]+)(=[^\\]]+)?\\]/g,\"[@$1$2]\",/\\s*,\\s*/g,\"|\",/\\s*(\\+|~|>)\\s*/g,\"$1\",/([a-zA-Z0-9\\_\\-\\*])~([a-zA-Z0-9\\_\\-\\*])/g,\"$1/following-sibling::$2\",/([a-zA-Z0-9\\_\\-\\*])\\+([a-zA-Z0-9\\_\\-\\*])/g,\"$1/following-sibling::*[1]/self::$2\",/([a-zA-Z0-9\\_\\-\\*])>([a-zA-Z0-9\\_\\-\\*])/g,\"$1/$2\",/\\[([^=]+)=([^'|\"][^\\]]*)\\]/g,\"[$1='$2']\",/(^|[^a-zA-Z0-9\\_\\-\\*])(#|\\.)([a-zA-Z0-9\\_\\-]+)/g,\"$1*$2$3\",/([\\>\\+\\|\\~\\,\\s])([a-zA-Z\\*]+)/g,\"$1//$2\",/\\s+\\/\\//g,\"//\",/([a-zA-Z0-9\\_\\-\\*]+):first-child/g, \"*[1]/self::$1\",/([a-zA-Z0-9\\_\\-\\*]+):last-child/g,\"$1[not(following-sibling::*)]\",/([a-zA-Z0-9\\_\\-\\*]+):only-child/g,\"*[last()=1]/self::$1\",/([a-zA-Z0-9\\_\\-\\*]+):empty/g,\"$1[not(*) and not(normalize-space())]\",/([a-zA-Z0-9\\_\\-\\*]+):not\\(([^\\)]*)\\)/g,function(c,a,b){return a.concat(\"[not(\",css2xpath(b).replace(/^[^\\[]+\\[([^\\]]*)\\].*$/g,\"$1\"),\")]\")},/([a-zA-Z0-9\\_\\-\\*]+):nth-child\\(([^\\)]*)\\)/g,function(c,a,b){switch(b){case \"n\":return a;case \"even\":return\"*[position() mod 2=0 and position()>=0]/self::\"+ a;case \"odd\":return a+\"[(count(preceding-sibling::*) + 1) mod 2=1]\";default:b=(b||\"0\").replace(/^([0-9]*)n.*?([0-9]*)$/,\"$1+$2\").split(\"+\");b[1]=b[1]||\"0\";return\"*[(position()-\".concat(b[1],\") mod \",b[0],\"=0 and position()>=\",b[1],\"]/self::\",a)}},/:contains\\(([^\\)]*)\\)/g,function(c,a){return\"[contains(string(.),'\"+a+\"')]\"},/\\[([a-zA-Z0-9\\_\\-]+)\\|=([^\\]]+)\\]/g,\"[@$1=$2 or starts-with(@$1,concat($2,'-'))]\",/\\[([a-zA-Z0-9\\_\\-]+)\\*=([^\\]]+)\\]/g,\"[contains(@$1,$2)]\",/\\[([a-zA-Z0-9\\_\\-]+)~=([^\\]]+)\\]/g, \"[contains(concat(' ',normalize-space(@$1),' '),concat(' ',$2,' '))]\",/\\[([a-zA-Z0-9\\_\\-]+)\\^=([^\\]]+)\\]/g,\"[starts-with(@$1,$2)]\",/\\[([a-zA-Z0-9\\_\\-]+)\\$=([^\\]]+)\\]/g,function(c,a,b){return\"[substring(@\".concat(a,\",string-length(@\",a,\")-\",b.length-3,\")=\",b,\"]\")},/\\[([a-zA-Z0-9\\_\\-]+)\\!=([^\\]]+)\\]/g,\"[not(@$1) or @$1!=$2]\",/#([a-zA-Z0-9\\_\\-]+)/g,\"[@id='$1']\",/\\.([a-zA-Z0-9\\_\\-]+)/g,\"[contains(concat(' ',normalize-space(@class),' '),' $1 ')]\",/\\]\\[([^\\]]+)/g,\" and ($1)\"],e=d.length;return function(c){for(var a= 0;a<e;)c=c.replace(d[a++],d[a++]);return\"//\"+c}}();";

            return Microsoft.JScript.Eval.JScriptEvaluate(originalJS + "; css2xpath('" + css + "')", Engine).ToString();
        }

        // Pretty little banner function: just for visual appeal :)
        // Feel free to steal it!
        private static void OutputBanner(String[] bannerLines) {
            int bannerWidth = Console.WindowWidth;
            int bannerPadding = 5;

            // Generate banner
            StringBuilder bannerBuilder = new StringBuilder();

            // Generate padding to prepend to each line
            String padding = "";
            bannerBuilder.Append(' ', bannerPadding);

            padding = bannerBuilder.ToString();

            // Reset string builer
            bannerBuilder.Clear();

            // Prepend padding for first line of stars
            bannerBuilder.Append(padding);

            // Add stars
            bannerBuilder.Append('*', bannerWidth - (bannerPadding * 2));
            bannerBuilder.AppendLine();

            // Generate each line
            foreach (String line in bannerLines) {
                String formattedLine = "";
                // Prepend padding and initial star
                formattedLine += padding + "*";

                // Calculate amount of space we can work with
                int lineWidth = bannerWidth - (bannerPadding * 2) - 2;

                // Calculate how much to pad in the stars
                int innerPadding = (lineWidth - line.Length) / 2;

                StringBuilder paddingBuilder = new StringBuilder();

                // Generate padding
                paddingBuilder.Append(' ', innerPadding);

                // Generate entire line
                formattedLine += paddingBuilder.ToString() + line + paddingBuilder.ToString();

                // Adjust for lines with an odd number of characters
                if (formattedLine.Length % 2 != 0)
                    formattedLine += " ";

                // Add final border star
                formattedLine += "*";

                // Add line to banner
                bannerBuilder.AppendLine(formattedLine);
            }

            // Finish off the last star border
            bannerBuilder.Append(padding);
            bannerBuilder.Append('*', bannerWidth - (bannerPadding * 2));
            bannerBuilder.AppendLine();

            // Display banner
            Console.WriteLine(bannerBuilder.ToString());
        }
    }
}
